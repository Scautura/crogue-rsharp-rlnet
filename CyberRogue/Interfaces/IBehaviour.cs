﻿namespace CyberRogue.Interfaces
{
    interface IBehaviour
    {
        bool Act(Core.Monster monster, Systems.CommandSystem commandSystem);
    }
}
