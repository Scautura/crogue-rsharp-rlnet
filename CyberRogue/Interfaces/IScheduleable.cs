﻿namespace CyberRogue.Interfaces
{
    interface IScheduleable
    {
        int Time { get; }
    }
}
