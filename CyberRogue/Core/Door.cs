﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CyberRogue.Interfaces;
using RLNET;
using RogueSharp;

namespace CyberRogue.Core
{
    class Door : IDrawable
    {
        public RLColor Color { get; set; }
        public RLColor BackGroundColor { get; set; }
        public char Symbol { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        public bool isOpen { get; set; }

        public Door()
        {
            Symbol = '+';
            Color = Colors.Door;
            BackGroundColor = Colors.DoorBackground;
        }

        public void Draw(RLConsole console, IMap map)
        {
            if (!map.GetCell(X, Y).IsExplored)
                return;

            Symbol = isOpen ? '-' : '+';
            if (map.IsInFov(X, Y))
            {
                Color = Colors.DoorFov;
                BackGroundColor = Colors.DoorBackgroundFov;
            }
            else
            {
                Color = Colors.Door;
                BackGroundColor = Colors.DoorBackground;
            }
            console.Set(X, Y, Color, BackGroundColor, Symbol);
        }
    }
}
